# API for gulpfile

## Functions
### newOrChanged(status)
Determine if status is new or changed.
* **status** { Object } - StatusObject from parse-git-status
* **status.x** { string } - Status code character
* **status.y** { string } - Status code character
* **status.to** { string } - The destination path (or just the path if not renamed)
* **status.from** { string } - The source path if a rename (null otherwise)
* @returns { boolean }

### getFileName(status)
Return file path from StatusObject.
* **status** { Object } - StatusObject from parse-git-status
* **status.x** { string } - Status code character
* **status.y** { string } - Status code character
* **status.to** { string } - The destination path (or just the path if not renamed)
* **status.from** { string } - The source path if a rename (null otherwise)

### callOnChanged(cb)
Call a function on any files that git status reveals to be new or changed
* **cb** { scan } - The function to call on any files that have changed

### writeFile(path, contents, cb)
Writes to file based on path, creating it and any required parent directories, if they don't exist
* **path** { string } - The path to the file to write to
* **contents** { string } - What to write to the file
* **cb** { Object } - The function to call once file has been written to

### scan(paths)
Perform Mocha accessibility tests on all files that have changed.
* **paths** { Array } - Paths to files that have changed

### encodePath(path)
Encode path to make safe any characters not allowed to be in directory names
* **path** { string } - Original path
* @returns { string } - Encoded path

### showPage(url, aXeA11yConf, tota11y)
Load a page in Chrome and run scans on it with tota11y library, using plugins created based on config file
* **url** { string } - The full URL to the page to show
* **aXeA11yConf** { [IConf](#iconf) \[\] } - Array of conf objects, one for each suite where page match was found in full conf file
* **tota11y** { string } - Path, from root of web server, to the tota11y library

### getPageSuite(fullConf, match)
Return a suite of configurations for any pages that had matches in the full configuration data
* **fullConf** { [IFullConf](#ifullconf) } - Full configuration data to be parsed
* **match** { string } - Path, relative to server root, of page you want to find matches for in the full conf
* @returns { [IConf](#iconf) \[\] } - Suite of configurations for any pages that have matches in the full configuration data

### savePageSuite(aXeA11yConf, path, cb)
Write to/create a file with a suite of configurations for any pages with matches in the full configuration
* **aXeA11yConf** { [IConf](#iconf) \[\] } - A suite of configuration objects
* **path** { string } - Path where you want to save the page suite
* **cb** { Object } - Function to call when conf file has been written

## Interfaces
### IMeta
 * **title** { string } - The title to be used when displaying results of scan
 * **description** { string } -  The description to be used when displaying results of scan

### IContext
 * **node** { Object } -
 * **selector** { string } - Selectors for elements to include or exclude in scan, only used if type is tags
 * **include** { any\[\] } - Selectors for elements to include in scan
 * **exclude** { any\[\] } - Selectors for elements to exclude from scan

### IncludeExclude
 * **include** {string\[\] } - Selectors for elements to include in scan
 * **exclude** {string\[\] } - Selectors for elements to exclude from scan

### IRunOnly
 * **type** { axe.RunOnlyType } - tag, rule, or tags
 * **value** { [IncludeExclude](#includeexclude) } - Selectors for elements to include or exclude in scan, only used if type is tags
 * **values** { string } - Used if type is tag or rule and contains either tag name or rule name, respectively

### IOptions
 * **runOnly** { [IRunOnly](#irunonly) } - Rules to run--if provided, then overrides defaults & runs only these and ones in rules
 * **rules** { Object } - Contains rules essentially of type axe.Rule, but I couldn't get that to work like I wanted

### IConf
 * **plugin** { [IMeta](#imeta) } - Specifies title and description of the scan to be run
 * **context** { [IContext](#icontext) } - Specifies elements to include or exclude in scan
 * **options** { [IOptions](#ioptions) } - Specifies which rules to run

### IFullConf
 * **files** { [IFile](#ifile) \[\] } - Specifies files that when matched cause suites of tests to be run
 * **once** { [IExpression](#iexpression) \[\] } - Specifies regular expressions that when matched cause suites of tests to be run
 * **templates** { [ITemplate](#itemplate) \[\] } - Templates contain conf objects that can be merged with templates, including pages

### IPage
 * **match** { string } - The path, relative to root of site, of a web page to be scanned
 * **template** { string } - The match property of another template object, which current one is merged with
 * { [IConf](#iconf) } conf - Provides configuration information used for a scan

### ISuite
 * @property { string } match - How suite is identified and matched
 * @property {IPage[] } pages - List of pages to be scanned, including how to scan them, and how to describe them

### IFile
 * **match** { string } - The path, relative to project root, of file that, when matched, causes suite to be added
 * **pages** { [IPage](#ipage) \[\] } - List of pages to be scanned, including how to scan them, and how to describe them

### IExpression
 * **match** { string } - Regular expression literal, converted to JSON-safe string, used to make suite, if matched
 * **once** { boolean } - Determines if, once matched, the expression is removed from list of expressions to match
 * **pages** { [IPage](#ipage) \[\] } - List of pages to be scanned, including how to scan them, and how to describe them

### ITemplate
 * **match** { string } - How the template is identified for matching purposes
 * **template** { string } - The match property of another template object, which current one is merged with
 * **conf** { [IConf](#iconf) } - Provides configuration information used for a scan
