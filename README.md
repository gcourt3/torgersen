# Torgersen Wordpress Theme

* [About](#about)
* [Technologies Used](#technologies-used)
* [Assumptions](#assumptions)
* [Prerequisites](#prerequisites)
* [Configuration](#configuration)
* [Install Node and Bower Packages](#install-node-and-bower-packages)
* [Gulp Build and Watch Commands](#gulp-build-and-watch-commands)
* [General Sage Documentation](#general-sage-documentation)
* [Accessibility Testing](#accessibility-testing)
  * [Possible Workflow](#possible-workflow)
  * [gulp scan](#gulp-scan)
    * [File Match Example](#file-match-example)
    * [Templates](#templates)
      * [Base Template](#base-template)
      * [Page That References Template](#page-that-references-template)
      * [Templates Matching Templates](#templates-matching-templates)
    * [Regular Expression Match Example](#regular-expression-match-example)
      * [Setting Regular Expression Suites to Only Run Once](setting-regular-expression-suites-to-only-run-once)
  * [gulp scanPage](#gulp-scanPage)
      

## About

The Torgersen Wordpress Theme is a [WordPress](https://wordpress.org/) theme designed from the ground up to be maximally [accessible](https://en.wikipedia.org/wiki/Web_accessibility) to people with many different abilities, such as blind/low vision individuals and those with motor or cognitive disabilities.  It is made available for use and modification by units at Virginia Tech.  It can be downloaded and used as is _(must add compiled version as zip eventually)_ or altered to suit the needs of your particular organization, if you have the technical skills to do so. 

## Technologies Used 

The Torgersen Wordpress Theme uses [version 8.5.0 of the Sage starter theme](https://github.com/roots/sage/tree/8.5.0) as its base, and is modeled after the [Moss template](http://sandbox.cms.vt.edu/moss.html) used by Virginia Tech's [Ensemble](http://ensemble.cms.vt.edu/) content management system.
 
Sage uses [Gulp](http://gulpjs.com/) as its build system and [Bower](http://bower.io/) to manage front-end packages.  It uses the official [Sass port of Twitter Bootstrap](https://github.com/twbs/bootstrap-sass) framework as the foundation for its responsive grid-based layout and most of its interactive widgets.

## Assumptions

To make changes to this theme, you should have a basic understanding of [WordPress theme development](https://developer.wordpress.org/themes/) generally and the [Sage starter theme](https://roots.io/sage/docs/) specifically, the [Bootstrap](getbootstrap.com) responsive design framework, the CSS extension language, [SASS](http://sass-lang.com/), the [NodeJS](https://nodejs.org) and [Bower](https://bower.io/) package management systems, and the [Gulp](http://gulpjs.com/) automation toolkit.  Tutorials on many of these topics are available from the [Viginia Tech-licensed Lynda training library](http://lynda.vt.edu). 

These instructions also assume you have a development instance of Wordpress running with our sample test data loaded into it.  Assistive Technologies makes available the [Assist Docker Wordpress development environment](https://git.it.vt.edu/rfentres/assist) based on the [Visible Wordpress Starter](https://github.com/visiblevc/wordpress-starter) and this comes pre-populated with the test data.

## Prerequisites

~~* Install Phantom JS as per instructions at [PhantomJS and Mac OS X](https://ariya.io/2012/02/phantomjs-and-mac-os-x) .  _Installing from brew recommended_~~  _This may no longer be necessary.  Must test and update instructions._

1. Install [node.js](http://nodejs.org/download/). We recommend you update to the latest version of npm: `npm install -g npm@latest`.
2. Install [gulp](http://gulpjs.com) and [Bower](http://bower.io/) globally with `npm install -g gulp bower`

## Configuration

In the Torgersen theme you've just checked out, locate the file [assets/manifest.json](assets/manifest.json) and make the following changes to it: 

Under `paths`, change the value of `root` to reflect the full path to the root of the project on your local development environment.
```
...
  "paths": {
    "root": "/path/to/torgersen/",
...
  }
```

_**Note:** In the code examples, the elipses, ```...```, represents code that has been ommitted from the example._

Update `devUrl` to reflect your local development host.

For example, if your local development URL is `http://192.168.99.100:8080` you would update the relevant section of the file to read:
```json
...
  "config": {
    "devUrl": "http://192.168.99.100:8080"
  }
...
```

If your local development URL looks like `http://localhost:8888/project-name/` you would update the file to read:
```json
...
  "config": {
    "devUrl": "http://localhost:8888/project-name/"
  }
...
```


## Install Node and Bower Packages

From the project root run:

1. `npm install`
2. `bower install`

You now have all the necessary dependencies to run the build process.

## Gulp Build and Watch Commands

_**Note:** Until you compile your assets with one of these commands, your site will appear broken when loaded in the browser_ 
* `gulp` — Compile and optimize the files in your assets directory
* `gulp watch` — Compile assets when file changes are made
* `gulp --production` — Compile assets for production (no source maps).

## General Sage Documentation

Learn more about how the Sage starter theme works in general by studying its [documentation](https://roots.io/sage/docs/).  Remember that currently we are working from version 8.5.0 of Sage; version 9+ handles things differently.

## Accessibility Testing

The Torgersen theme is designed to be modified by units on campus to suit their own needs, but has been designed to help ensure that any modifications made to the theme meet the same same high standards for accessibility as the original.  To this end, accessibility testing functionality has been integrated into the theme's build system with a couple of gulp tasks:
 * [gulp scan](#gulp-scan)
 * [gulp scanPage](#gulp-scanpage)

### Possible Workflow

The workflow for using these commands might be something like this:

1. Commit changes to your repository, which triggers _gulp scan_.  This basically gives a report that says, "You are committing a file whose filename has matched a defined test suite.  We've run scans on the following pages that are built from or use this file, and here are a list of accessibility rules that have been violated."  This gives you a quick sense that something is broken.
2. To then see that problem in context, you would run _gulp scanPage_ on a particular URL that was flagged in _gulp scan_.  This will load the page in the browser and display a widget that shows the results of scans associated with that page.  The user can see all those reports in context on the page that triggered them, and, by clicking on a violation that is listed in a report, have the part of the page that triggered the violation be highlighted.

### gulp scan

When the user commits changes to the repository, the _gulp scan_ task is called, running some [Mocha](https://mochajs.org/)  tests. These tests work by loading pages in the [PhantomJS](http://phantomjs.org/) headless browser and use Selenium's [WebDriverJS](https://github.com/SeleniumHQ/selenium/wiki/WebDriverJs) to dynamically inject the [aXe JavaScript accessibility testing library](https://github.com/dequelabs/axe-core) into these pages.  aXe will scan specific sections of relevant pages and the results of these tests will be output to the console and will also be published as a web page to [test/reports](test/reports) using the [MochAwesome](http://adamgruber.github.io/mochawesome/) reporter.

Exactly what scans get run is determined by the configuration file, [test/specs/full.json](test/specs/full.json).  Basically, _gulp scan_ will look at each file that has changed in this commit and see if it matches any of the selectors for test suites specified in the configuration file.  Each test [suite](API.md#isuite), can be triggered by matching either an exact file name or a regular expression.  Suites designed to be triggered based on an exact file name match are stored in the `files` array, while suites to be triggered based on a regular expression match are stored in the `expressions` array.  For each test suite, one or more web pages can be scanned based on [page objects](API.md#ipage) defined in the `suite`.  The [options object](API.md#ioptions), describing which accessibility rules will be checked against, and the [context object](API.md#icontext), describing which parts of the page will be examined, are both specified in the [conf object](API.md#iconf) associated with the `page`.  The syntax of the `context` and `options` objects is based on the [aXe Javascript Accessibility API](https://github.com/dequelabs/axe-core/blob/master/doc/API.md)'s [context parameter](https://github.com/dequelabs/axe-core/blob/master/doc/API.md#context-parameter), and [options parameter](https://github.com/dequelabs/axe-core/blob/master/doc/API.md#options-parameter), respectively.

#### File Match Example

In [test/specs/full.json](test/specs/full.json), a [file test suite](API.md#ifile) may be defined that matches a particular filename, by setting the suite's `match` property to the path to that file from the root of the project.  For instance, for a particular commit, the file, [search.php](search.php), may have changed and there may be a suite in the full configuration whose `match` property matches that filename.

```json
{
  "files": [
    {
      "match": "search.php",
      ...
    }
  ]
}
```

Since search.php is used whenever the home page is accessed with the _"s"_ URL parameter, we will want to create a `page` object in that test `suite` that matches that URL, so that a scan is run on that page.  I know, given the [Wordpress unit test data](https://codex.wordpress.org/Theme_Unit_Test), that a search for _"test"_ will return a list of results, so my first `page` object matches the URL with that string as the search query.

```json
{
  "files": [
    {
      "match": "search.php",
      "pages": [
        {
          "match": "/s=test"
          ...
        }
      ]
    }
  ]
}
```

However, I also know that search.php returns a different view if my query string doesn't return any results, so I'll need to add a `page` object to match that too.  In that case, I would set the _"s"_ URL parameter to the garbage string, _"kjhgkjhgkj"_, which I know won't return anything.

```json
{
  "files": [
    {
      "match": "search.php",
      "pages": [
        {
          "match": "/s=test"
          ...
        },
        {
          "match": "/s=kjhgkjhgkj"
          ...
        }
      ]
    }
  ]
}
```


Also, in this case, in order to get the most useful feedback, we will only want to scan the relevant `context`, in this case, the part of the page that is generated by search.php, which is contained in the element with the id, `"#template-search"`.  Similarly, we will not need to run certain tests, such as whether the page has a title, since that is set in the `<head>` tag, which is beyond the scope of what search.php affects. Again, what sections of the page to `include` and `exclude` and what specific `rules` to test for is determined by the `conf` object.

```json
{
  "files": [
    {
      "match": "search.php",
      "pages": [
        {
          "match": "/s=test",
          "conf": {
            "context": {
              "include": [["#template-search"]]
            },
            "options": {
              "rules": {
                "document-title": { "enabled": false },
                "meta-refresh": { "enabled": false },
                "meta-viewport-large": { "enabled": false },
                "meta-viewport": { "enabled": false }
              }
            }
          }
        },
        ...
      ]
    },
    ...
  ]
}
```

For details of how to specify what parts of the page to scan and what rules to include, see the documentation for the axe-core library's[context parameter](https://github.com/dequelabs/axe-core/blob/master/doc/API.md#context-parameter) and [options parameter](https://github.com/dequelabs/axe-core/blob/master/doc/API.md#options-parameter).  Generally, if it can be serialized to a JSON object, you can use the syntax specified there.

#### Templates

Some configurations will be the same for many scans so, rather than create duplicate `conf` objects for all of them, we can create a single [template object](API.md#itemplate) defining a `conf` object common to all of them.  Any `page` objects that reference that `template` will inherit its `conf` object properties.  Templates are defined in the `templates` array in the configuration file.  A `page` references a `template` by setting its `template` property to the value of the template's `match` property.

_**Note:** The word "template" as used when referring to the `template` objects in the templates section of the config file has nothing to do with way the word "template" is used in common Wordpress parlance.  They are separate concepts._

As an example, many scans will want to use, as their base, rules [tagged](https://github.com/dequelabs/axe-core/blob/master/doc/rule-descriptions.md) as being associated with "WCAG 2.0 A", "WCAG 2.0 AA", and "Best Practice".  We can create a template called _"base"_ that defines these base rules that are to be used and have our `page` object reference that.

##### Base Template

```json
{
  "files": [
    ...
  ],
  "templates": [
    {
      "match": "base",
      "conf": {
        "options": {
          "runOnly": {
            "type": "tag",
            "values": ["wcag2a", "wcag2aa", "best-practice"]
          }
        }
      }
    }
  ]
}
```

##### Page That References Template

```json
{
  "files": [
    {
      "match": "templates/footer.php",
      "pages": [
        {
          "match": "/",
          "template": "base",
          "conf": {
            "context": {
              "include": [["footer#vt_footer_wrapper"]]
            },
            "rules": {
              "document-title": { "enabled": false }
            }
          }
        }
      ]
    }
  ],
  "templates": [
    ...
  ]
 }
```

In our example, we still have a `conf` object associated with the `page` object, even though that `page` object is also referencing the `template`.  In this case, the `conf` object contained in the `page` object supplements the `conf` object in the `template`, by specifying that the _"document-title"_ rule is to be excluded from the scan.  It also limits the `context` so that only the contents of our page's primary footer tag will be scanned.

##### Templates matching templates

`page` objects can reference `template` objects, but `template` objects can also reference other `template` objects, inheriting properties from them. Knowing this, we could eliminate the `conf` object from the `page` object altogether and just reference a _"footer"_ `template` object, which in turn inherits from the _"base"_ `template`.

```json
{
  "files": [
    ...
    "match": "templates/footer.php",
    "pages": [
      {
        "match": "/",
        "template": "footer"
      },
      ...
    ]
  ]
  "templates": [
    {
      "match": "footer",
      "template": "base",
      "conf": {
        "context": {
          "include": [["footer#vt_footer_wrapper"]]
        },
        "rules": {
          "document-title": { "enabled": false }
        }
      }
    },
    {
      "match": "base",
      "conf": {
        "options": {
          "runOnly": {
            "type": "tag",
            "values": ["wcag2a", "wcag2aa", "best-practice"]
          }
        }
      }
    }
  ]
}
```

#### Regular Expression Match Example
 
 In our previous examples, we showed how a suite of tests can be triggered by a filename match, but you can also use a regular [expression](API.md#iexpression) to trigger a match.  For instance, you can write a regular expression that matches any file ending in _"scss"_, `"/^(?!\\.\\.\\/).*\\.scss$/i"`, and any time one of those files change, you can scan a representative sample of pages using a template that looks at the whole page, but only tests rules that would be affected by styling changes, such as _"color-contrast"_.

```json
{
  "expressions": [
    {
      "match": "/^(?!\\.\\.\\/).*\\.scss$/i",
      "pages": [
        {
          "match": "/about/page-markup-and-formatting/",
          "template": "scss"
        }
      ]
    }
  ],
  "templates": [
    {
      "match": "scss",
      "conf": {
        "options": {
          "runOnly": {
            "type": "rule",
            "values": ["button-name", "color-contrast", "link-in-text-block", "link-name"]
          }
        }
      }
    }
  ]
}
```

##### Setting Regular Expression Suites to Only Run Once

Depending on your purposes, for a particular set of file names in a commit, you may want a regular expression suite to only be run the first time a file in that set is matched, or every time.  This option is set using the `once` property in the regular expression `suite`.  So, for instance, if you wanted to run a suite of tests of JavaScript-related functionality only once, even if there were multiple JavaScript files in your changeset, you would set `once` to the boolean value `true`, like so:

```
    {
      "match": "/^(assets\\/scripts\\/).*\\.js$/i",
      "once": true,
      "pages": [
        {
          "match": "/",
          "template": "js"
        }
      ]
    }
```

The default value for `once` is `false`, so, unless otherwise specified, a regular expression suite will be run every time it is matched.  File suites, by contrast, do not have a `once` property and are always run only once.

### gulp scanPage

In addition to running scans automatically based on files that have changed since the last commit to the master branch, the user can choose to run a scan on a particular page using the `gulp scanPage` command passing in the `match` parameter for the page to be scanned.  So, to scan the home page, you would run the following:
```
gulp scanPage --match=/
```
Running this type of scan looks through all the `suites` in the full config file for any `page` objects whose `match` property is the URL referenced and, for each one, runs a scan based on its associated `conf` object.  Thus, several separate scans may be performed on a single page, each taken from a separate `suite`.  For instance, there may be one that scans the footer with a certain set of rules, one that scans the header with a different set of rules, and so forth.  When this type of scan is run, the specified URL is opened in the browser and the results displayed as plugins of the [tota11y accessibility visualization toolkit](https://git.it.vt.edu/rfentres/tota11y), a collapsible widget that is injected into the page.

