(function($) {
  // Site title
  wp.customize('blogname', function(value) {
    value.bind(function(to) {
      $('.site-title a').text(to);
    });
  });
  // Site description (tagline)
  wp.customize('blogdescription', function(value) {
    value.bind(function(to) {
      $('.site-description').text(to);
    });
  });

  // Abbreviation to use for site title on extra-small breakpoints
  wp.customize('title_abbr', function(value) {
    value.bind(function(to) {
      console.log(to.length);
      $('.title-abbr').text(to);
      if (to.length > 0) {
        if (!$('.title-abbr').hasClass('visible-xs-inline')) {
          $('.title-abbr').addClass('visible-xs-inline');
          $('.title-abbr').removeClass('hidden-xs').removeClass('hidden');
        }
        if (!$('.title-full').hasClass('hidden-xs')) {
          $('.title-full').addClass('hidden-xs');
          $('.title-full').removeClass('visible-xs-inline');
        }
      } else {
        if (!$('.title-abbr').hasClass('hidden')) {
          $('.title-abbr').addClass('hidden');
          $('.title-abbr').removeClass('hidden-xs').removeClass('visible-xs-inline');
        }
        if (!$('.title-full').hasClass('visible-xs-inline')) {
          $('.title-full').addClass('visible-xs-inline');
          $('.title-full').removeClass('hidden-xs');
        }
      }
    });
  });

})(jQuery);
