/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
        var minWidth = 768;
        enquire.register("screen and (min-width: "+minWidth+"px)", {
          setup: function() {
            var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
            if (w < minWidth) {
              $("[role='banner'] [name='vtsearchform']").prependTo("#site-navigation .collapse");
            }
          },
          match : function() {
            $("#site-navigation .collapse [name='vtsearchform']").appendTo("[role='banner'] .search");
          },
          unmatch : function() {
            $("[role='banner'] [name='vtsearchform']").prependTo("#site-navigation .collapse");
          }
        });
        var menu = $('#site-navigation');
        var belowMenu = $('#primary');
        var origOffsetY = menu.offset().top;

        /*function scroll() {
          if ($(window).scrollTop() > origOffsetY) {
            menu.addClass('navbar-fixed-top');
            belowMenu.addClass('menu-padding');
          } else {
            if (menu.hasClass('navbar-fixed-top')) {
              menu.removeClass('navbar-fixed-top');
              belowMenu.removeClass('menu-padding');
            }
          }

        }
        document.onscroll = scroll;*/
        /*$(window).resize(function () {
          menu.removeClass('navbar-fixed-top');
          belowMenu.removeClass('menu-padding');
          scroll();
        });*/

        function scrollElementTop() {
          var selector = "#"+location.hash.substring(1);
          var offset = $(selector).offset().top;
          if (!/^(?:a|select|input|button|textarea)$/i.test($(selector).prop("tagName"))) {
            $(selector).attr('tabIndex', '-1');
            $(selector).css('outline', 0);
          }
          /*if ($(selector).offset().top >= origOffsetY) {
            var navbarHeight = 50;
            var fontSizeBase = 15;
            var lineHeightBase = 1.8;
            var lineHeightComputed = Math.floor((fontSizeBase * lineHeightBase));
            offset -= navbarHeight + lineHeightComputed;
          }*/
          $('html, body').animate({ scrollTop: offset }, 1);
          $(selector).focus();
        }
        window.addEventListener("hashchange", scrollElementTop);

        $("a[href^='#']").not("a[href='#']").click(scrollElementTop);
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    },
    // Pages with sidebars.
    'sidebar_primary': {
      init: function() {
        // JavaScript to be fired on the about us page

      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
