<?php
use Roots\Sage\Setup;
use Roots\Sage\Wrapper;
?>
<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!--[if IE]>
    <div class="alert alert-warning">
      <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
    </div>
    <![endif]-->
    <div class="wrap">
    <?php
      do_action('get_header');
      get_template_part('templates/header');
    ?>
    <div id="primary" class="wrap">
      <?php if (Setup\display_breadcrumb()) : ?>
      <nav aria-label="Breadcrumb Nav" class="row">
        <?php get_template_part('templates/breadcrumb'); ?>
      </nav>
      <?php endif; ?>

      <?php if (Setup\display_content_header_before()) { ?>
      <div class="content-header-before">
        <div class="container" id="contentHeaderBefore">
          <div class="row">
            <div class="content">
              <?php get_template_part('templates/page', 'header'); ?>
            </div>
         </div>
        </div>
        <?php if ( has_post_thumbnail() ) : ?>
        <div class="full-bleed-header">
          <?php get_template_part('templates/page', 'header-image'); ?>
        </div>
        <?php endif; ?>
      </div>
      <?php } ?>

      <div class="conditional-container">
        <div class="row">

          <?php if (Setup\display_sidebar()) : ?>
          <div class="sidebar">
            <?php include Wrapper\sidebar_path(); ?>
          </div><!-- /.sidebar -->
          <?php endif; ?>

          <main id="main" role="main" class="main">
              <div class="row">

                <?php if (Setup\display_page_nav_before()) : ?>
                <div class="sidebar break-before">
                  <?php dynamic_sidebar('page-nav');  ?>
                </div><!-- /.break-before -->
                <?php endif; ?>

                <div class="content">
                  <?php include Wrapper\template_path(); ?>
                </div><!-- /.content -->

                <?php if (Setup\display_secondary_sidebar()) : ?>
                <div class="sidebar">
                <?php
                  if (!Setup\display_page_nav_before()) {
                    dynamic_sidebar('page-nav');
                  }
                  dynamic_sidebar('supplementary');
                ?>
                </div><!-- /.sidebar -->
                <?php endif; ?>

              </div>
          </main><!-- /.main -->

        </div><!-- /.row -->
      </div><!-- /.conditional-container -->
    </div>
    </div><!-- /.wrap -->
    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>
  </body>
</html>
