<?php
use Roots\Sage\Setup;
?>
<?php while (have_posts()) : the_post(); ?>
  <?php if (!Setup\display_content_header_before()) {
    get_template_part('templates/page', 'front_header');
    if ( has_post_thumbnail() ) {
      get_template_part('templates/page', 'header-image');
    }
  }
  get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>
