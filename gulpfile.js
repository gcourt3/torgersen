// ## Globals

var gulp          = require('gulp');
var argv          = require('minimist')(process.argv.slice(2));
var autoprefixer  = require('gulp-autoprefixer');
var browserSync   = require('browser-sync').create();
var changed       = require('gulp-changed');
var concat        = require('gulp-concat');
var flatten       = require('gulp-flatten');
var gulpif        = require('gulp-if');
var imagemin      = require('gulp-imagemin');
var jshint        = require('gulp-jshint');
var lazypipe      = require('lazypipe');
var less          = require('gulp-less');
var merge         = require('merge-stream');
var cssNano       = require('gulp-cssnano');
var plumber       = require('gulp-plumber');
var rev           = require('gulp-rev');
var runSequence   = require('run-sequence');
var sass          = require('gulp-sass');
var sourcemaps    = require('gulp-sourcemaps');
var uglify        = require('gulp-uglify');
var _             = require('lodash');
var gitrev        = require('git-rev');
var git           = require('gulp-git');
var fs            = require('graceful-fs');
var mkdirp        = require('mkdirp');
var shell         = require('shelljs');
var findParentDir = require('find-parent-dir');
var pathModule    = require('path');
var mocha         = require('gulp-spawn-mocha');

var gulpFilter    = require('gulp-filter');
var guppy         = require('git-guppy')(gulp);

var gitStatus     = require('parse-git-status');

var parseConf     = require("parse-conf");

// See https://github.com/austinpray/asset-builder
var manifest      = require('asset-builder')('./assets/manifest.json');

require('chromedriver');
var WebDriver     = require('selenium-webdriver');
var until         = WebDriver.until;
var by            = WebDriver.By;

var gitParent;
try {
  gitParent = findParentDir.sync(__dirname, '.git');
} catch(err) {
  console.error('error', err);
}

var gulpParent    = pathModule.resolve();
var gulpRelToGit  = pathModule.relative(gitParent, gulpParent);

// `path` - Paths to base asset directories. With trailing slashes.
// - `path.source` - Path to the source files. Default: `assets/`
// - `path.dist` - Path to the build directory. Default: `dist/`
// - `path.test` - Path to the mocha test directory.
var path = manifest.paths;
var testPath = pathModule.join(path.root, path.test);

// `config` - Store arbitrary configuration values here.
var config = manifest.config || {};
var testUrl = config.devUrl+"/"+path.theme+path.test;

// `globs` - These ultimately end up in their respective `gulp.src`.
// - `globs.js` - Array of asset-builder JS dependency objects. Example:
//   ```
//   {type: 'js', name: 'main.js', globs: []}
//   ```
// - `globs.css` - Array of asset-builder CSS dependency objects. Example:
//   ```
//   {type: 'css', name: 'main.css', globs: []}
//   ```
// - `globs.fonts` - Array of font path globs.
// - `globs.images` - Array of image path globs.
// - `globs.bower` - Array of all the main Bower files.
var globs = manifest.globs;

// `project` - paths to first-party assets.
// - `project.js` - Array of first-party JS assets.
// - `project.css` - Array of first-party CSS assets.
var project = manifest.getProjectGlobs();

// CLI options
var enabled = {
  // Enable static asset revisioning when `--production`
  rev: argv.production,
  // Disable source maps when `--production`
  maps: !argv.production,
  // Fail styles task on error when `--production`
  failStyleTask: argv.production,
  // Fail due to JSHint warnings only when `--production`
  failJSHint: argv.production,
  // Strip debug statments from javascript when `--production`
  stripJSDebug: argv.production
};

// Path to the compiled assets manifest in the dist directory
var revManifest = path.dist + 'assets.json';

/**
 * Determine if status is new or changed.
 * @param {Object} status - StatusObject from parse-git-status
 * @param {string} status.x - Status code character
 * @param {string} status.y - Status code character
 * @param {string} status.to - The destination path (or just the path if not renamed)
 * @param {string} status.from - The source path if a rename (null otherwise)
 * @returns {boolean}
 */
function newOrChanged(status) {
  return ((status.y==="M") || (status.y==="A") || (status.y==="?") || (status.y==="U"));
}

/**
 * Return file path from StatusObject.
 * @param {Object} status - StatusObject from parse-git-status
 * @param {string} status.x - Status code character
 * @param {string} status.y - Status code character
 * @param {string} status.to - The destination path (or just the path if not renamed)
 * @param {string} status.from - The source path if a rename (null otherwise)
 */
function getFileName(status) {
  return status.to;
}

/**
 * Call a function on any files that git status reveals to be new or changed
 * @param {scan} cb - The function to call on any files that have changed
 */
function callOnChanged(cb) {
  git.status({args: '--porcelain -z'}, function (err, stdout) {
    if (err) {
      throw err;
    } else {
      var changeLog = gitStatus(stdout);
      cb(_.map(_.filter(changeLog, newOrChanged), getFileName));
    }
  });
}

/**
 * Writes to file based on path, creating it and any required parent directories, if they don't exist
 * @param {string} path - The path to the file to write to
 * @param {string} contents - What to write to the file
 * @param {Object} cb - The function to call once file has been written to
 */
function writeFile(path, contents, cb) {
  mkdirp(pathModule.dirname(path), function (err) {
    if (err) {
      return cb(err);
    }

    fs.writeFile(path, contents, cb);
  });
}

/**
 * Loads Mochawesome report in Chrome
 */
function loadReport() {
  var driver = new WebDriver.Builder().forBrowser("chrome").build();
  driver.get("file://"+ process.cwd()+'/test/reports/mochawesome.html')
    .then(function () {
      console.log("Report loaded in Chrome.  Check browser.");
      driver.wait(until.titleIs(undefined));
    });
}

/**
 * Perform accessibility scan.
 * @callback scan
 * @param {Array} paths - Paths to files that have changed
 */
/**
 * Perform accessibility scan.
 * @param {Array} paths - Paths to files that have changed
 */
function scan(paths) {
  /*
   If you get an EACCESS error when running scan,
   run sudo chmod -R a+rwx node_modules/gulp-spawn-mocha from the root of the project
   */
  var fullConf = require('./'+path.test+'specs/full.json');
  var parsedPath = pathModule.join(path.test, 'conf/parsed.json');

  var parser = new parseConf.Parse(fullConf);
  parser.createSuites(paths);
  parser.expandTemplates();

  var parsedConf = JSON.stringify(parser.fullSuite, null, 4);

  writeFile(parsedPath, parsedConf, function(err) {
    if (err) {
      throw err;
    }
    if (parser.fullSuite.length > 0) {
      return gulp.src(['test/specs/test.js'])
        .pipe(mocha({
          env: {
            'MOCHAWESOME_REPORTDIR': 'test/reports'
          },
          R: 'mochawesome'
        })
        .on('end', function () {
          loadReport();
        })
        .on('error', function () {
          loadReport();
        }));
    } else {
      console.log("No matches in configuration file.");
      return true;
    }
  });
}

/**
 * Encode path to make safe any characters not allowed to be in directory names
 * @param {string} path - Original path
 * @returns {string} - Encoded path
 */
function encodePath(path) {
  var pathAry = path.split("/");
  var end = encodeURIComponent(pathAry.pop());
  pathAry.push(end);
  return pathAry.join("/");
}

/**
 * Load a page in Chrome and run scans on it with tota11y library, using plugins created based on config file
 * @param {string} url - The full URL to the page to show
 * @param {IConf[]} aXeA11yConf - Array of conf objects, one for each suite where page match was found in full conf file
 * @param {string} tota11y - Path, from root of web server, to the tota11y library
 */
function showPage(url, aXeA11yConf, tota11y) {

  console.log(JSON.stringify(aXeA11yConf, null, "  "));
  console.log(tota11y);

  var driver = new WebDriver.Builder()
    .forBrowser("chrome")
    .build();

  driver.get(url)
    .then(function () {

      console.log("Adding tota11y script to page.");
      driver.executeScript(
        "window.aXeA11y = { multi: false }; " +
        "window.aXeA11y.conf = " + JSON.stringify(aXeA11yConf) + "; " +
        "var s=window.document.createElement('script'); " +
        "s.src='/"+tota11y+"'; window.document.head.appendChild(s)");

      console.log("Waiting until tota11y toolbar is added to page.");
      driver.wait(until.elementLocated(by.className("tota11y")));

      console.log("Expanding toolbar.");
      driver.findElement(by.className("tota11y-toolbar-toggle")).click();

      console.log("Leaving window open.");
      driver.wait(until.titleIs(undefined));

    });
}

/**
 * Return a suite of configurations for any pages that had matches in the full configuration data
 * @param {IFullConf} fullConf - Full configuration data to be parsed
 * @param {string} match - Path, relative to server root, of page you want to find matches for in the full conf
 * @returns {IConf[]} - Suite of configurations for any pages that have matches in the full configuration data
 */
function getPageSuite(fullConf, match) {
  var parser = new parseConf.Parse(fullConf);
  return parser.getPageSuite(match);
}

/**
 * Write to/create a file with a suite of configurations for any pages with matches in the full configuration
 * @param {IConf[]} aXeA11yConf - A suite of configuration objects
 * @param {string} path - Path where you want to save the page suite
 * @param {Object} cb - Function to call when conf file has been written
 */
function savePageSuite(aXeA11yConf, path, cb) {

  writeFile(path, JSON.stringify(aXeA11yConf), function(err) {
    if (err) {
      return console.error(err);
    } else {
      console.log('Successfully created page conf at: ' + path);
      return cb();
    }
  });
}

gulp.task('scan', ['cleanreport'], function() {
  // callOnChanged(scan);
  return scan([
/*    "templates/breadcrumb.php",
    "templates/comments.php",
    "templates/content.php",
    "templates/content-single.php",
    "templates_custom/full.php",
    "templates_custom/full-bc.php",
    "templates_custom/left-bc.php",
    "templates_custom/right.php",
    "templates_custom/right-bc.php",
    "templates_custom/right-break.php",
    "templates_custom/three-col.php",
    "templates_custom/three-col-bc.php",
    "templates/entry-meta.php",
    "templates/footer.php",
    "templates/head.php",
    "templates/header.php",
    "search.php",*/
    "404.php",
    "assets/styles/x.scss"
  ]);
});

// Called with gulp scanPage --match=/
// where match is the path from the base url, including the initial slash
gulp.task('scanPage', function() {
  var match = argv.match;
  var parsedPath = pathModule.join("test/conf", encodePath(match), "conf.json");
  var url = config.devUrl + match;

  var fullConf      = require('./'+path.test+'specs/full.json');

  var tota11y = "tota11y.js";
  if (fs.existsSync(revManifest)) {
    var man = require("./" + revManifest);
    tota11y = man[tota11y];
  }
  var tota11yPath = pathModule.join(path.theme, path.dist, "scripts", tota11y);

  // TODO Check if there is already page suite file and use that if full conf file isn't newer
  var aXeA11yConf = getPageSuite(fullConf, match);

  savePageSuite(aXeA11yConf, parsedPath, function() {});

  showPage(url, aXeA11yConf, tota11yPath);

});

gulp.task('cleanreport', require('del').bind(null, [path.test+'reports/*']));

gulp.task('pre-commit', guppy.src('pre-commit', function (files) {
  console.log('pre-commit');
  gitrev.branch(function (str) {
    if (str === "master") {

      files = _.map(files, function(file) {
        var fileDir = pathModule.resolve(pathModule.dirname(file));
        var relFileDir = pathModule.relative(gulpRelToGit, fileDir);

        return pathModule.format({
          dir: relFileDir,
          base: pathModule.basename(file)
        });
      });

      scan(files);
    }
  });
}));

gulp.task('publish', function(callback) {
  runSequence('publish:clean', 'publish:cp', 'publish:cd', 'publish:add', 'publish:commit', 'publish:push', callback);
});

gulp.task('publish:clean', require('del').bind(null, ['../torgersen-published/**', '!../torgersen-published', '!../torgersen-published/.git/', '!../torgersen-published/.git/**'], {force: true}));

//copy files to torgersen-published directory
gulp.task('publish:cp', function() {
  return gulp.src(['./**/*', '!.*', '!.*/**',
    '!assets/', '!assets/**', '!bower_components/', '!bower_components/**', '!node_modules/', '!node_modules/**',
    '!test/', '!test/**', '!tmp/', '!tmp/**', '!bower.json', '!CHANGELOG.md', '!LICENSE.md', '!README.md',
    '!helper.js', '!gulpfile.js', '!package.json', '!ruleset.xml', '!torgersen.iml', '!parse-conf.js'])
    .pipe(gulp.dest('../torgersen-published'));
});

//change to torgersen-published directory
gulp.task('publish:cd', function() {
  return shell.cd('../torgersen-published');
});

//add files to for commit
gulp.task('publish:add', function() {
  return gulp.src(shell.pwd().stdout)
    .pipe(git.add({args: '-A'}));
});

//commit files
gulp.task('publish:commit', function() {
  var now = new Date();
  return gulp.src(shell.pwd().stdout)
    .pipe(git.commit('Build for ' + now.getMonth() + '-' + now.getDate() + '-' + now.getFullYear() + ', ' + now.getHours() + ':' + now.getMinutes() + ':' + now.getSeconds()));
});

gulp.task('publish:push', function() {
  git.push('origin', 'master', function (err) {
    if (err) {
      throw err;
    }
  });
});



// ## Reusable Pipelines
// See https://github.com/OverZealous/lazypipe

/**
 * Process SASS, LESS, & CSS (create sourcemaps, compile, concatenate, add necessary prefixes, minify and revision)
 * @example
 * gulp.src(cssFiles)
 *  .pipe(cssTasks('main.css')
 *  .pipe(gulp.dest(path.dist + 'styles'))
 * @param filename - path to file that is to be processed
 * @returns {*}
 */
var cssTasks = function(filename) {
  return lazypipe()
    .pipe(function() {
      return gulpif(!enabled.failStyleTask, plumber());
    })
    .pipe(function() {
      return gulpif(enabled.maps, sourcemaps.init());
    })
    .pipe(function() {
      return gulpif('*.less', less());
    })
    .pipe(function() {
      return gulpif('*.scss', sass({
        outputStyle: 'nested', // libsass doesn't support expanded yet
        precision: 10,
        includePaths: ['.'],
        errLogToConsole: !enabled.failStyleTask
      }));
    })
    .pipe(concat, filename)
    .pipe(autoprefixer, {
      browsers: [
        'last 2 versions',
        'android 4',
        'opera 12'
      ]
    })
    .pipe(cssNano, {
      safe: true
    })
    .pipe(function() {
      return gulpif(enabled.rev, rev());
    })
    .pipe(function() {
      return gulpif(enabled.maps, sourcemaps.write('.', {
        sourceRoot: 'assets/styles/'
      }));
    })();
};

/**
 * Process JavaScript (create sourcemaps, concatenate, uglify and revision)
 * @example
 * gulp.src(jsFiles)
 *  .pipe(jsTasks('main.js')
 *  .pipe(gulp.dest(path.dist + 'scripts'))
 * @param filename - path to file that is to be processed
 * @returns {*}
 */
var jsTasks = function(filename) {
  return lazypipe()
    .pipe(function() {
      return gulpif(enabled.maps, sourcemaps.init());
    })
    .pipe(concat, filename)
    .pipe(uglify, {
      compress: {
        'drop_debugger': enabled.stripJSDebug
      }
    })
    .pipe(function() {
      return gulpif(enabled.rev, rev());
    })
    .pipe(function() {
      return gulpif(enabled.maps, sourcemaps.write('.', {
        sourceRoot: 'assets/scripts/'
      }));
    })();
};

/**
 * Write any revved files to the rev manifest.
 * See <a hrfe="https://github.com/sindresorhus/gulp-rev">https://github.com/sindresorhus/gulp-rev</a>
 * @summary Write to rev manifest
 * @param directory - Where to look for revved files
 * @returns {*}
 */
var writeToManifest = function(directory) {
  return lazypipe()
    .pipe(gulp.dest, path.dist + directory)
    .pipe(browserSync.stream, {match: '**/*.{js,css}'})
    .pipe(rev.manifest, revManifest, {
      base: path.dist,
      merge: true
    })
    .pipe(gulp.dest, path.dist)();
};

// ## Gulp tasks
// Run `gulp -T` for a task summary

// ### Styles
// `gulp styles` - Compiles, combines, and optimizes Bower CSS and project CSS.
// By default this task will only log a warning if a precompiler error is
// raised. If the `--production` flag is set: this task will fail outright.
gulp.task('styles', ['wiredep'], function() {
  var merged = merge();
  manifest.forEachDependency('css', function(dep) {
    var cssTasksInstance = cssTasks(dep.name);
    if (!enabled.failStyleTask) {
      cssTasksInstance.on('error', function(err) {
        console.error(err.message);
        this.emit('end');
      });
    }
    merged.add(gulp.src(dep.globs, {base: 'styles'})
      .pipe(cssTasksInstance));
  });
  return merged
    .pipe(writeToManifest('styles'));
});

// ### Scripts
// `gulp scripts` - Runs JSHint then compiles, combines, and optimizes Bower JS
// and project JS.
gulp.task('scripts', ['jshint'], function() {
  var merged = merge();
  manifest.forEachDependency('js', function(dep) {
    merged.add(
      gulp.src(dep.globs, {base: 'scripts'})
        .pipe(jsTasks(dep.name))
    );
  });
  return merged
    .pipe(writeToManifest('scripts'));
});

// ### Fonts
// `gulp fonts` - Grabs all the fonts and outputs them in a flattened directory
// structure. See: https://github.com/armed/gulp-flatten
gulp.task('fonts', function() {
  return gulp.src(globs.fonts)
    .pipe(flatten())
    .pipe(gulp.dest(path.dist + 'fonts'))
    .pipe(browserSync.stream());
});

// ### Images
// `gulp images` - Run lossless compression on all the images.
gulp.task('images', function() {
  return gulp.src(globs.images)
    .pipe(imagemin({
      progressive: true,
      interlaced: true,
      svgoPlugins: [{removeUnknownsAndDefaults: false}, {cleanupIDs: false}]
    }))
    .pipe(gulp.dest(path.dist + 'images'))
    .pipe(browserSync.stream());
});

// ### JSHint
// `gulp jshint` - Lints configuration JSON and project JS.
gulp.task('jshint', function() {
  return gulp.src([
    'bower.json', 'gulpfile.js'
  ].concat(project.js))
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(gulpif(enabled.failJSHint, jshint.reporter('fail')));
});

// ### Clean
// `gulp clean` - Deletes the build folder entirely.
gulp.task('clean', require('del').bind(null, [path.dist]));


// ### Watch
// `gulp watch` - Use BrowserSync to proxy your dev server and synchronize code
// changes across devices. Specify the hostname of your dev server at
// `manifest.config.devUrl`. When a modification is made to an asset, run the
// build step for that asset and inject the changes into the page.
// See: http://www.browsersync.io
gulp.task('watch', function() {
  browserSync.init({
    files: ['{lib,templates}/**/*.php', '*.php'],
    proxy: {
      target:config.devUrl
    },
    snippetOptions: {
      whitelist: ['/wp-admin/admin-ajax.php'],
      blacklist: ['/wp-admin/**']
    }
  });
  gulp.watch([path.source + 'styles/**/*'], ['styles']);
  gulp.watch([path.source + 'scripts/**/*'], ['jshint', 'scripts']);
  gulp.watch([path.source + 'fonts/**/*'], ['fonts']);
  gulp.watch([path.source + 'images/**/*'], ['images']);
  gulp.watch(['bower.json', 'assets/manifest.json'], ['build']);
});

// ### Build
// `gulp build` - Run all the build tasks but don't clean up beforehand.
// Generally you should be running `gulp` instead of `gulp build`.
gulp.task('build', function(callback) {
  runSequence('styles',
    'scripts',
    ['fonts', 'images'],
    callback);
});

// ### Wiredep
// `gulp wiredep` - Automatically inject Less and Sass Bower dependencies. See
// https://github.com/taptapship/wiredep
gulp.task('wiredep', function() {
  var wiredep = require('wiredep').stream;
  return gulp.src(project.css)
    .pipe(wiredep())
    .pipe(changed(path.source + 'styles', {
      hasChanged: changed.compareSha1Digest
    }))
    .pipe(gulp.dest(path.source + 'styles'));
});

// ### Gulp
// `gulp` - Run a complete build. To compile for production run `gulp --production`.
gulp.task('default', ['clean'], function() {
  gulp.start('build');
});
