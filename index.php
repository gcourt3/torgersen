<?php
// This is the default page and will display pages or posts if primary templates for these haven't been defined
?>
<?php get_template_part('templates/page', 'header'); ?>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>
<?php while (have_posts()) : the_post(); ?>
  <?php
  // If the post type is post, then treat it as an archive and loop through all posts, inserting content.php, which displays excerpts.
  //  Otherwise, append the post type to content and load that.  So if content type was page, it would insert content-page.php.
  get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format());
  ?>
<?php endwhile; ?>

<?php
the_posts_pagination( array(
  'screen_reader_text' => 'Pagination Navigation',
  'prev_text'          => __( '« Previous', 'sage' ),
  'next_text'          => __( 'Next »', 'sage' ),
  'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'sage' ) . ' </span>',
) );
?>
