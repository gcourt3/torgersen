<?php

namespace Roots\Sage\Customizer;

use Roots\Sage\Assets;

/**
 * Add postMessage support
 */
function customize_register($wp_customize) {
  $wp_customize->get_setting('blogname')->transport = 'postMessage';
  $wp_customize->add_setting('title_abbr', [
    'type' => 'option',
    'default' => 'auto',
    'transport' => 'postMessage'
  ]);
  $wp_customize->add_control('title_abbr', [
    'label' => __('Site Title Abbreviation', 'sage'),
    'description' => __('Site title to display on extra-small breakpoints'),
    'section' => 'title_tagline',
    'type' => 'text'
  ]);
}
add_action('customize_register', __NAMESPACE__ . '\\customize_register');

/**
 * Customizer JS
 */
function customize_preview_js() {
  wp_enqueue_script('sage/customizer', Assets\asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
}
add_action('customize_preview_init', __NAMESPACE__ . '\\customize_preview_js');

