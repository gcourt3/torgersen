<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }

    if ( has_post_thumbnail() ) {
      $classes[] = 'has-post-thumbnail';
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  // Add class if secondary sidebar is active
  if (Setup\display_secondary_sidebar()) {
    $classes[] = 'sidebar-secondary';
  }

  // Add class if page nav is supposed to break before content on small viewports
  if (Setup\display_page_nav_before()) {
    $classes[] = 'page-nav-before';
  }

  if (in_array("sidebar-primary", $classes) && in_array("sidebar-secondary", $classes)) {
    $classes[] = 'three-col';
  }

  // Add class if page nav is supposed to break before content on small viewports
  if (Setup\display_breadcrumb()) {
    $classes[] = 'breadcrumb-displayed';
  }

  // Add class if only the first footer sidebar has content
  if (Setup\display_single_footer()) {
    $classes[] = 'single-footer';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '" title="Continue reading '. get_the_title() . '">' . __('Continue', 'sage') . ' <span class="sr-only">reading '. get_the_title() . '</span></a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');
