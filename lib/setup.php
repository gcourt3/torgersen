<?php

namespace Roots\Sage\Setup;

use Roots\Sage\Assets;

/**
 * Theme setup
 */
function setup() {

  /**
   * Configuration values
   */
  if (!defined('WP_ENV')) {
    // Fallback if WP_ENV isn't defined in your WordPress config
    // Used in lib/assets.php to check for 'development' or 'production'
    define('WP_ENV', 'production');
  }

  if (!defined('DIST_DIR')) {
    // Path to the build directory for front-end assets
    define('DIST_DIR', '/dist/');
  }

  /**
   * Enable theme features
   */
  add_theme_support('soil-clean-up');         // Enable clean up from Soil
  add_theme_support('soil-nav-walker');
  add_theme_support('soil-relative-urls');    // Enable relative URLs from Soil
  add_theme_support('soil-nice-search');      // Enable nice search from Soil
  add_theme_support('soil-jquery-cdn');       // Enable to load jQuery from the Google CDN
  add_theme_support('bootstrap-gallery');     // Enable Bootstrap's thumbnails component on [gallery]

  // Make theme available for translation
  // Community translations can be found at https://github.com/roots/sage-translations
  load_theme_textdomain('sage', get_template_directory() . '/lang');

  // Enable plugins to manage the document title
  // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
  add_theme_support('title-tag');

  // Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus
  register_nav_menus([
    'primary_navigation' => __('Primary Navigation', 'sage'),
    'secondary_navigation' => __('Secondary Navigation', 'sage')
  ]);

  // Add post thumbnails
  // http://codex.wordpress.org/Post_Thumbnails
  // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
  // http://codex.wordpress.org/Function_Reference/add_image_size
  add_theme_support('post-thumbnails');

  // Add post formats
  // http://codex.wordpress.org/Post_Formats
  add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

  // Add HTML5 markup for captions
  // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
  add_theme_support('html5', ['caption', 'comment-form', 'comment-list']);

  // Tell the TinyMCE editor to use a custom stylesheet
//  add_editor_style(Assets\asset_path('styles/editor-style.css'));
}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

/**
 * Register sidebars
 */
function widgets_init() {
  register_sidebar([
    'name'          => __('Section Nav', 'sage'),
    'id'            => 'section-nav',
    'before_widget' => '<nav class="widget %1$s %2$s" aria-label="Section Nav">',
    'after_widget'  => '</nav>',
    'before_title'  => '<h2>',
    'after_title'   => '</h2>'
  ]);
  register_sidebar([
    'name'          => __('Section Info', 'sage'),
    'id'            => 'section-info',
    'before_widget' => '<aside class="navbar widget %1$s %2$s" aria-label="Section Info">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h2>',
    'after_title'   => '</h2>'
  ]);

  register_sidebar([
    'name'          => __('Jumbotron', 'sage'),
    'id'            => 'jumbotron',
    'before_widget' => '<div class="widget %1$s %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '',
    'after_title'   => ''
  ]);

  register_sidebar([
    'name'          => __('Supplemental Nav', 'sage'),
    'id'            => 'page-nav',
    'before_widget' => '<nav class="widget %1$s %2$s" aria-label="Supplemental Nav">',
    'after_widget'  => '</nav>',
    'before_title'  => '<h2>',
    'after_title'   => '</h2>'
  ]);
  register_sidebar([
    'name'          => __('Supplemental Info', 'sage'),
    'id'            => 'supplementary',
    'before_widget' => '<aside class="navbar widget %1$s %2$s" aria-label="Supplemental Info">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h2>',
    'after_title'   => '</h2>'
  ]);


  register_sidebar([
    'name'          => __('Footer', 'sage'),
    'id'            => 'sidebar-footer',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h2>',
    'after_title'   => '</h2>'
  ]);
  register_sidebar([
    'name'          => __('Footer 1', 'sage'),
    'id'            => 'sidebar-footer1',
    'before_widget' => '<section class="widget %1$s %2$s footermenu">',
    'after_widget'  => '</section>',
    'before_title'  => '<h2>',
    'after_title'   => '</h2>'
  ]);
  register_sidebar([
    'name'          => __('Footer 2', 'sage'),
    'id'            => 'sidebar-footer2',
    'before_widget' => '<section class="widget %1$s %2$s footermenu">',
    'after_widget'  => '</section>',
    'before_title'  => '<h2>',
    'after_title'   => '</h2>'
  ]);
  register_sidebar([
    'name'          => __('Footer 3', 'sage'),
    'id'            => 'sidebar-footer3',
    'before_widget' => '<section class="widget %1$s %2$s footermenu">',
    'after_widget'  => '</section>',
    'before_title'  => '<h2>',
    'after_title'   => '</h2>'
  ]);
  register_sidebar([
    'name'          => __('Footer 4', 'sage'),
    'id'            => 'sidebar-footer4',
    'before_widget' => '<section class="widget %1$s %2$s footermenu">',
    'after_widget'  => '</section>',
    'before_title'  => '<h2>',
    'after_title'   => '</h2>'
  ]);
}
add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');

/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar() {
  static $display;

  isset($display) || $display = !in_array(true, [
    // The sidebar will NOT be displayed if ANY of the following return true.
    // @link https://codex.wordpress.org/Conditional_Tags
    is_404(),
    is_front_page(),
    is_page_template([
      'templates_custom/full.php',
      'templates_custom/fluid.php',
      'templates_custom/full-bc.php',
      'templates_custom/section-right.php',
      'templates_custom/right.php',
      'templates_custom/right-bc.php',
      'templates_custom/right-break.php']),
    is_search()
  ]);

  return apply_filters('sage/display_sidebar', $display);
}

/**
 * Determine which pages should display the secondary sidebar
 */
function display_secondary_sidebar() {
  static $display_secondary;

  isset($display_secondary) || $display_secondary = in_array(true, [
    is_page_template([
      'templates_custom/section-right.php',
      'templates_custom/right.php',
      'templates_custom/right-bc.php',
      'templates_custom/section-three-col.php',
      'templates_custom/three-col.php',
      'templates_custom/three-col-bc.php',
      'templates_custom/right-break.php'])
  ]);

  return apply_filters('sage/display_secondary_sidebar', $display_secondary);
}

function display_page_nav_before() {
  static $break_before;

  isset($break_before) || $break_before = in_array(true, [
    is_page_template(['templates_custom/right-break.php'])
  ]);

  return apply_filters('sage/display_page_nav_before', $break_before);
}

/**
 * Determine which pages should display the secondary sidebar
 */
function display_breadcrumb() {
  static $display_bc;

  isset($display_bc) || $display_bc = in_array(true, [
    is_page_template([
      'templates_custom/left-bc.php',
      'templates_custom/right-bc.php',
      'templates_custom/three-col-bc.php',
      'templates_custom/full-bc.php'])
  ]);

  return apply_filters('sage/display_breadcrumb', $display_bc);
}

/**
 * Determine if only the first footer sidebar has content
 */
function display_single_footer() {
  static $display_single_footer;

  isset($display_single_footer) || $display_single_footer = (
    !is_active_sidebar( 'sidebar-footer' ) &&
    is_active_sidebar( 'sidebar-footer1' ) &&
    !is_active_sidebar( 'sidebar-footer2' ) &&
    !is_active_sidebar( 'sidebar-footer3' ) &&
    !is_active_sidebar( 'sidebar-footer4' )
  );
  return apply_filters('sage/display_single_footer', $display_single_footer);
}

/**
 * Determine whether page header should before content and sidebars
 */
function display_content_header_before() {
  static $display_header;

  isset($display_header) || $display_header = in_array(true, [
    is_front_page(),
    is_page_template([
      'templates_custom/section-right.php',
      'templates_custom/section-left.php',
      'templates_custom/section-three-col.php'])
  ]);

  return apply_filters('sage/display_content_header_before', $display_header);
}


function assets() {
  wp_enqueue_style('sage_css', Assets\asset_path('styles/main.css'), false, null);
  wp_enqueue_style('bootstrap_accessibility_plugin', Assets\asset_path('styles/bootstrap-accessibility-plugin.css'), false, null);

  if (is_single() && comments_open() && get_option('thread_comments')) {
    wp_enqueue_script('comment-reply');
  }

  wp_enqueue_script('modernizr', Assets\asset_path('scripts/modernizr.js'), [], null, true);
  wp_enqueue_script('enquire', Assets\asset_path('scripts/enquire.js'), [], null, true);
  wp_enqueue_script('sage_js', Assets\asset_path('scripts/main.js'), ['jquery'], null, true);
  if ( is_admin_bar_showing() ) {
    wp_enqueue_script('tota11y', Assets\asset_path('scripts/tota11y.js'), ['jquery'], null, true);
  }
  wp_enqueue_script('bootstrap_accessibility_plugin', Assets\asset_path('scripts/bootstrap-accessibility-plugin.js'), [], null, true);
}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);
function load_conf() {
  if ( is_admin_bar_showing() ) {
    echo '
    <script> 
    var aXeA11y = 
        {
                "multi": false,
                "conf": [
                {
                    "meta": {
                        "title": "Accessibility tests",
                        "description": "Click to check all rules."
                    },
                    "context": {
                        "exclude": [
                            ["#wpadminbar"],["#querylist"]
                        ]
                    }
                }]
        };
    </script>';
  }
}
add_action('wp_head', __NAMESPACE__ . '\\load_conf');
