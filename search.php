<div id="template-search">
  <?php get_template_part('templates/page', 'header'); ?>

  <?php if (!have_posts()) : ?>
    <div class="alert alert-warning">
      <?php _e('Sorry, no results were found.', 'sage'); ?>
    </div>
  <?php endif; ?>

  <?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/content', 'search'); ?>
  <?php endwhile; ?>

  <?php
  the_posts_pagination( array(
    'screen_reader_text' => 'Pagination Navigation',
    'prev_text'          => __( '« Previous', 'sage' ),
    'next_text'          => __( 'Next »', 'sage' ),
    'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'sage' ) . ' </span>',
  ) );
  ?>
</div>
