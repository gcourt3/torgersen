<?php
use Roots\Sage\Titles;
?>

<header class="page-title">
  <h1 class="entry-title"><?= Titles\title(); ?></h1>
  <?php get_template_part('templates/entry-meta'); ?>
</header>
