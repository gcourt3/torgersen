<?php
use Roots\Sage\Setup;
?>
<div class="template-content-single">
  <?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>
    <?php
    if (!Setup\display_content_header_before()) {
      get_template_part('templates/content-single', 'header');
      if ( has_post_thumbnail() ) {
        get_template_part('templates/page', 'header-image');
      }
    }
    get_template_part('templates/content-single', 'page');
    ?>
  </article>
  <?php endwhile; ?>
</div>
