<?php
// This outputs an excerpt for use on an archive page
?>
<div class="template-content">
  <article <?php post_class(); ?>>
    <header>
      <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
      <?php get_template_part('templates/entry-meta'); ?>
    </header>
    <div>
      <?php the_excerpt(); ?>
    </div>
  </article>
</div>
