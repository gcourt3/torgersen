<?php
use Roots\Sage\Setup;
// This is called by all the custom templates
?>
<div class="vt-torg-custom-page">
<?php
if (!Setup\display_content_header_before()) {
  get_template_part('templates/page', 'header');
  if ( has_post_thumbnail() ) {
    get_template_part('templates/page', 'header-image');
  }
}
get_template_part('templates/content', 'page');
?>
</div>


