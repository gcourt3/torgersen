<?php
use Roots\Sage\Setup;
?>
<div id="vt-footer-wrapper">
  <div class="vt-footer-container container">
<div class="pseudo-row">
    <div class="vt-torg-footer-nav-group">
      <div class="vt-torg-footer-nav-wrapper">
        <nav class="single" aria-label="Footer Nav">
          <?php dynamic_sidebar('sidebar-footer1'); ?>
          <?php if (!Setup\display_single_footer()) : ?>
            <?php dynamic_sidebar('sidebar-footer2'); ?>
            <?php dynamic_sidebar('sidebar-footer3'); ?>
            <?php dynamic_sidebar('sidebar-footer4'); ?>
          <?php endif; ?>
        </nav>
      </div>
    </div>
    <div class="row vt-torg-footer-contentinfo-group">
      <div class="vt-torg-footer-contentinfo-wrapper">
        <footer role="contentinfo">
          <?php if (!Setup\display_single_footer()) : ?>
            <div class="logo-wrapper"><img src="<?= get_template_directory_uri();?>/dist/images/logo.svg" alt="Virginia Tech: Invent the Future" class="logo"></div>
            <?php dynamic_sidebar('sidebar-footer'); ?>
          <?php endif; ?>
          <p class="copyright">&copy; 2016 Virginia Polytechnic Institute and State University</p>
        </footer>
      </div>

  </div>
</div>
