<?php
use Roots\Sage\Nav\NavWalker;
use Roots\Sage\Setup;

$title_abbr = get_option('title_abbr', '');
$description = get_bloginfo( 'description', 'display' );
$skiptarget = "#contentHeader";
if (Setup\display_content_header_before()) {
  $skiptarget = "#contentHeaderBefore";
}

?>
<div id="header">
  <nav id="skips" aria-label="Skip Navigation"><a href="<?= $skiptarget ?>" class="sr-only sr-only-focusable">Skip to Content</a></nav>

  <header role="banner" class="banner">
    <div class="container">
      <div class="row">
        <div class="logo-block">
          <div class="divider"><a href="http://vt.edu" title="Virginia Tech Home" class="brand" aria-labelledby="logo">
            <img src="<?= get_template_directory_uri(); ?>/dist/images/logo.svg" alt="Virginia Tech: Invent the Future"
                 class="logo hidden-sm" id="logo">
            <img src="<?= get_template_directory_uri(); ?>/dist/images/shield.svg" alt="Virginia Tech: Invent the Future" class="logo-shield">
          </a></div>
        </div><!-- DON'T REMOVE COMMENT HACK!!!!! (Opening of comment must be immediately after closing div and closing of comment must be immediately before opening div. No spaces!)
     --><div class="department-name">
          <div class="site-branding">
            <div class="site-title"><a href="<?php echo esc_url(home_url('/')); ?>"><?php bloginfo('name'); ?></a></div>
            <?php if ( $description || is_customize_preview() ) : ?>
            <p class="site-description"><?php echo $description; ?></p>
            <?php endif; ?>
          </div>
        </div><!-- DON'T REMOVE COMMENT HACK!!!!! (Opening of comment must be immediately after closing div and closing of comment must be immediately before opening div. No spaces!)
     --><div class="search">
          <?php get_search_form(); ?>
        </div>
        </div>
    </div>
  </header>

  <nav aria-label="Site Menu" id="site-navigation" class="navbar navbar-inverse main-navigation" role="navigation">
    <div class="container">
      <div style="float:left"><a href="http://vt.edu" title="Virginia Tech Home" class="brand" aria-labelledby="logo">
          <img src="<?= get_template_directory_uri(); ?>/dist/images/shield.svg" alt="Virginia Tech: Invent the Future" class="logo-shield"></a></div>
      <div class="navbar-header">
        <a href="<?= esc_url(home_url('/')); ?>" class="navbar-brand shrinky visible-xs" style="display:block;">
        <?php if ($title_abbr) : ?><!-- User has provided a abbreviation to use on xs viewports -->
          <span class="title-abbr visible-xs-inline" ><?php echo $title_abbr ?></span>
          <span class="title-full hidden-xs" ><?php bloginfo('name'); ?></span>
        <?php elseif (is_customize_preview()) : ?><!-- If using customizer, title-abbr needs to be there, even if empty-->
          <span class="title-abbr hidden" ><?php echo $title_abbr ?></span>
          <span class="title-full" ><?php bloginfo('name'); ?></span>
        <?php else: ?><!-- No title-abbr provided and not in customizer, so just display regular title-->
          <?php bloginfo('name'); ?>
        <?php endif; ?>
        </a>
        <button type="button" class="navbar-toggle collapsed btn-xs" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only"><?= __('Toggle navigation', 'sage'); ?></span>
          <span class="fa fa-bars" aria-hidden="true"></span> <span>Main Menu</span>
        </button>
      </div>
      <div class="navbar-collapse collapse">
        <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(['theme_location' => 'primary_navigation', 'walker' => new NavWalker(), 'menu_class' => 'nav navbar-nav']);
        endif;
        ?>
      </div>
    </div>
  </nav>
</div>

