<?php
use Roots\Sage\Titles;
?>

<div class="page-title">
  <h1 class="sr-only"><?= Titles\title(); ?></h1>
</div>
