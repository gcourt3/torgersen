<?php
$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
$thumbnail_alt = get_post_meta($post_thumbnail_id, '_wp_attachment_image_alt', true);
$thumbnail_src = wp_get_attachment_image_src( $post_thumbnail_id, 'full' );
$thumbnail_srcset = wp_get_attachment_image_srcset( $post_thumbnail_id );
$thumbnail_sizes = wp_get_attachment_image_sizes( $post_thumbnail_id );
$thumbnail_url = $thumbnail_src[0];
$thumbnail_height = $thumbnail_src[2];
?>

<figure class="header-image">
  <img src="<?php echo esc_url( $thumbnail_url ); ?>"
       srcset="<?php echo esc_attr($thumbnail_srcset); ?>"
       size="<?php echo esc_attr(wp_get_attachment_image_sizes( $post_thumbnail_id )); ?>"
       alt="<?php echo esc_attr($thumbnail_alt) ?>"
       style="max-height:<?php echo esc_attr($thumbnail_height) ?>px; ">
</figure>
