<form class="navbar-form" role="search" action="<?= esc_url(home_url('/')); ?>" method="get" name="vtsearchform" autocomplete="off">
  <div class="form-group">
    <div class="input-group">
      <label for="vt_search_box" class="sr-only"><?php _e('Search for:', 'sage'); ?></label>
      <input type="text" maxlength="50" value="<?= get_search_query(); ?>" placeholder="<?php _e('Search', 'sage'); ?>" name="s" id="vt_search_box" class=" form-control" required>
      <span class="input-group-btn">
        <button id="vt_go_button" type="submit" class="btn btn-default">
          <span class="glyphicon glyphicon-search"></span>
          <span class="sr-only"><?php _e('Search', 'sage'); ?></span>
        </button>
      </span>
    </div>
  </div>
</form>
