<?php
/**
* Template Name: Full Width with Breadcrumb
*/
?>
<div class="vt-torg-custom-wrapper" id="vt-torg-temp-custom-full-bc">
  <?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/custom', 'page'); ?>
  <?php endwhile; ?>
</div>
