<?php
/**
* Template Name: Full Width
*/
?>
<div class="vt-torg-custom-wrapper" id="vt-torg-temp-custom-full">
  <?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/custom', 'page'); ?>
  <?php endwhile; ?>
</div>
