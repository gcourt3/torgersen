<?php
/**
 * Template Name: Right Col with Breadcrumb
 */
?>
<div class="vt-torg-custom-wrapper" id="vt-torg-temp-custom-right-bc">
  <?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/custom', 'page'); ?>
  <?php endwhile; ?>
</div>
