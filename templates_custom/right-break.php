<?php
/**
 * Template Name: Right Column, Nav Breaks Before
 */
?>
<div class="vt-torg-custom-wrapper" id="vt-torg-temp-custom-right-break">
  <?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/custom', 'page'); ?>
  <?php endwhile; ?>
</div>

