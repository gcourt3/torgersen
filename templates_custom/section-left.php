<?php
/**
 * Template Name: Site Section - Left Column
 */
?>
<div class="vt-torg-custom-wrapper" id="vt-torg-temp-custom-left">
  <?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/custom', 'page'); ?>
  <?php endwhile; ?>
</div>
