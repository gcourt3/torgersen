<?php
/**
 * Template Name: Three Column
 */
?>
<div class="vt-torg-custom-wrapper" id="vt-torg-temp-custom-three-col">
  <?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/custom', 'page'); ?>
  <?php endwhile; ?>
</div>
