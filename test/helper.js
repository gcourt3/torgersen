global.parsed = require('./conf/parsed.json'); //JSON.parse(process.env.suite);

global.WebDriver    = require('selenium-webdriver');
global.AxeBuilder   = require('axe-webdriverjs');
var rules           = require('axe-core').getRules();
global.description = function (id) {
  for (var i = 0; i < rules.length; i++) {
    if (rules[i].ruleId === id) {
      return rules[i].description;
    }
  }
}
global._ = require('lodash');

global.expect = require('chai').expect;
global.manifest = require('./../assets/manifest.json');
global.devUrl = manifest.config.devUrl;
