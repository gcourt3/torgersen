var driver,
  profile = [
    { id: "Galaxy S5", w: 360, h: 640 },
    { id: "iPhone 5", w: 320, h: 568 },
    { id: "iPhone 6", w: 375, h: 667 },
    { id: "iPhone 6 Plus", w: 414, h: 736 },
    { id: "iPad", w: 768, h: 1024 },
    { id: "iPad Pro", w: 1024, h: 1366 }
  ];

function checkPage(page) {
  if (page.conf.options.runOnly.type === 'rule') {
    page.conf.options.runOnly.values.forEach(function (rule) {
      it(description(rule), function (done) {
        var AB = AxeBuilder(driver);
        if (typeof page.conf.context !== "undefined") {
          if (page.conf.context.include) AB.include(page.conf.context.include);
          if (page.conf.context.exclude) AB.exclude(page.conf.context.exclude);
        }
        AB.withRules(rule);
        AB.analyze(function (results) {
          expect(results.violations.length).to.equal(0);
          done();
        });
      });
    });
  } else {
    it("should violate no axe-core rules", function (done) {
      AxeBuilder(this.driver)
        .analyze(function (results) {
          expect(results.violations.length).to.equal(0);
          done();
        });
    });
  }
}

before(function (done) {
  driver = new WebDriver.Builder()
    .forBrowser('chrome')
    .build();
  done();
});

after(function (done) {
  driver.quit().then(function () {
    done();
  });
});
parsed.forEach(function (suite) {
  describe("Accessibility of " + suite.match, function () {
    suite.pages.forEach(function (page) {

      context("as used on page " + page.match + " in context: " + page.conf.meta.title, function () {
        before(function (done) {
          driver.get(devUrl + page.match)
            .then(function () {
              done();
            });
        });
        profile.forEach( function(device) {
          context("as viewed on device: " + device.id, function () {
            context("in portrait mode: " + device.w + "x" + device.h, function () {
              before(function (done) {
                driver.manage().window().setSize(device.w, device.h).then(function () {
                  done();
                });
              });
              checkPage(page);
            });
            context("in landscape mode: " + device.h + "x" + device.w, function () {
              before(function (done) {
                driver.manage().window().setSize(device.h, device.w).then(function () {
                  done();
                });
              });
              checkPage(page);
            });

          });
        });
      });
    });
  });
});
